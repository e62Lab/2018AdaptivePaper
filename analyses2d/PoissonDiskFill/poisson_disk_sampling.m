%seed node
x = 0.5;
y = 0.5;
types = 0; %type 0 means it hasn't expand yet

e_1 = 0.6; %search toleranceKd

clc;close all
r_target = @(x, y) 0.01 + x*0.01 + y*0.01;

% r_target = @(x, y) ((x-0.5)^2 + (y-0.5)^2)*0.05 + 0.015;
tic;
createFig('ps','m2 1x2 p2', 'scale', 1);hold on
p = plot(x, y,'+');

q = plot(x,y, 'ro');
axis([0 1.2 0 1.2]);

break_= 0;
iter = 5000; %number of iterations
freq = 5000;
tic
for i = 1:iter;
   if ((mod(i, freq) == 0 || i == iter))
        toc;p.XData = x;p.YData = y;
        disp(i/iter);pause(0.01);tic;
   end
    
    %select node from not expanded nodes
    expand_candidates = find(types == 0);
    if (isempty(expand_candidates)) p.XData = x;p.YData = y;break;end
    
    r_i = expand_candidates(ceil(rand(1)*length(expand_candidates)));
 
    q.XData = [q.XData, x(r_i)]; 
    q.YData = [q.YData, y(r_i)];
  
    types(r_i) = 1; %set node to expanded status
    r_0 = r_target(x(r_i), y(r_i)); %collect desired spatial step

    %uniformly ditribute nodes
    N = 6; %number of new nodes
    f = 0:2*pi/N:2*pi; %angles
    x_n = r_target(x(r_i), y(r_i)) * cos(f)' + x(r_i);
    y_n = r_target(x(r_i), y(r_i)) * sin(f)' + y(r_i);
    
    %add nodes
    [supp, d] = knnsearch([x, y], [x_n, y_n], 'k', 1);
    
    ii = d(:,1) < r_0*e_1 | x_n > 1 | x_n < 0 | y_n > 1 | y_n < 0;
    x_n(ii) = []; %remove nodes that do not fullfill the adding criteria
    y_n(ii) = []; %remove nodes that do not fullfill the adding criteria
  
    x = [x; x_n];
    y = [y; y_n];
    types = [types; zeros(size(x_n))]; %new nodes are expandable
end
toc
size(x)