prepare

casename = 'hertzian2';
load([plotdatapath casename '.mat'])

ax = linspace(-2*a, 2*a, 1000);
ay = 0*ax;
[asxx, asyy, asxy] = hertzian_analytical(ax, ay, a, p0);

niter = size(data,1);
iterdata = zeros(niter, 4);
for i = 1:niter
%     if i == 4, continue, end
    v = von_mises(data{i}.sxx, data{i}.syy, data{i}.sxy);

    x = data{i}.x;
    y = data{i}.y;

    iterdata(i, 1) = data{i}.N;
    iterdata(i, 2) = data{i}.errinf;
    iterdata(i, 3) = data{i}.err1;
    iterdata(i, 4) = data{i}.errE;
    if (mod(i, 2) == 1 || i == niter)

    I = find(y == max(y));
    f2 = setfig(['bo' sprintf('%d', i)], [400, 300]);
    if i >= 4, ms = 3; elseif i==3, ms=6; else, ms = 10; end
    plot(ax/a, asxx/p0, '-r');
%     plot(FF(:, 1)*1e3, FF(:, 3)/1e6, '-');
    plot(data{i}.x(I)/a, data{i}.sxx(I)/p0, '.-k', 'MarkerSize', ms);
    xlabel('$x/a$')
    ylabel('$\sigma_{xx}/p_0$')
    xlim([-2, 2])
    if i == 1, ylim([-2, 0.2]), else, ylim([-1.5, 0.3]), end
    title(sprintf('iteration %d, $N = %d$', i-1, length(data{i}.x)))
    legend('analytical solution', 'adaptive solution', 'Location', 'SE')
    
%     exportf(f2, [imagepath casename '_top_traction' sprintf('%d', i) '.pdf'])
    
%     break
    end
end

f2 = setfig('b2', [600 450]);
f = 5e3;
uu = data{i}.uu';
vv = data{i}.vv';
scatter((x+f*uu)/a, (y+f*vv)/a, 4, v/p0, 'filled');
axis equal
colormap jet
colorbar
xlim([-3, 3])
ylim([-4, -0.5])
caxis([0 1])
xlabel('$x/a$')
ylabel('$y/a$')
title(sprintf('von Mises stress / $p_0$, $N = %d$', data{niter}.N))
% exportf(f2, [imagepath casename '_von_mises.png'])


f1 = setfig('b3', [400 300]);
% k = polyfit(log(iterdata(:, 1)), log(iterdata(:, 2)), 1);
% plot(iterdata(:, 1), iterdata(:, 2), 'o-');
plot(0:6, iterdata(:, 2), 'x-');
plot(0:6, iterdata(:, 3), 'o-');
plot(0:6, iterdata(:, 4), '<-');
% plot(iterdata(:, 1), exp(k(2))*iterdata(:, 1).^k(1), 'k--');
% set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
xlabel('iteration number')
ylabel('error')
legend('$e_\infty$', '$e_1$', '$e_E$', 'Location', 'SW');
xlim([-1 7])
% xlim([2000, 10^5])
% ylim([0.01, 1.2])
% xticks([5000, 10000, 50000, 100000])
% xticklabels({'$5\cdot10^3$', '$10^4$', '$5\cdot10^4$', '$10^5$'})
% xlim([5, 10^5])

% exportf(f1, [imagepath casename '_error.pdf'])

f1 = setfig('bo9', [400 300]);
plot(0:6, iterdata(:, 1), 'o-');
set(gca, 'YScale', 'log');
xlabel('iteration number')
ylabel('$N$')
ylim([5e3, 6e4])
xlim([-1 7])
yticks([5e3, 8e3, 1e4, 2e4, 3e4, 5e4])

% exportf(f1, [imagepath casename '_nodes.pdf'])

f4 = setfig('b4');
load([plotdatapath 'hertzian_refine_levels_convergence.mat'])
rl = size(data, 1);
plot(iterdata(:, 1), iterdata(:, 2), 'o-');
for i = 1:rl
    plot(data(i, :, 1), data(i, :, 2), 'o-')
end
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');