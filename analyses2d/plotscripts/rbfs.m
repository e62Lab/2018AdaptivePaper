prepare

n = 5;
rng(684)

A = rand(n, 2);
x = A(:, 1);
y = A(:, 2);

f1 = setfig('b1', [600, 400]); hold off
scatter3(x, y, ones(size(x)), 40, 'filled', 'k') 
hold on

sigma = 0.5;
eps = 1/sigma;
den = 300;
for i = 1:n
    xs = linspace(x(i)-1/eps, x(i)+1/eps, den);
    ys = linspace(y(i)-1/eps, y(i)+1/eps, den);
    [X, Y] = meshgrid(xs, ys);
    surf(X, Y, exp(-((X-x(i)).^2 + (Y-y(i)).^2)*eps^2),...
        'EdgeColor', 'none');
end
d = 0.5;
xlim([0, 1.35])
ylim([-d, 1.5])
% title(sprintf('Gaussian radial basis functions with $\\varepsilon = %.0f$', eps))

colormap parula

exportf(f1, [imagepath 'rbfs.png'], '-density', 300)