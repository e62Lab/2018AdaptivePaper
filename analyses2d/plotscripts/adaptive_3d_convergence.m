prepare
casename = 'adaptive_3d';

load([plotdatapath casename '.mat'], 'data');
data = cell2mat(data);
iter = 0:length(data)-1;

f1 = setfig('b1', [600 350]);
plot(iter, [data(:).einfs], '-x');
plot(iter, [data(:).e1s], '-<');
plot(iter, [data(:).ees], '-o');
plot(iter, [data(:).einfu], '-h', 'Color', [0.4 0.4 0.4]);
plot(iter, [data(:).e1u], '-+');
set(gca, 'yscale', 'log');
ylim([5e-3 5])
ylabel('error')
xticks(iter)

yyaxis right
plot(iter, [data(:).N], '-*', 'Color', [0.4940    0.1840    0.5560]);
set(gca, 'ycolor', [0.4940    0.1840    0.5560])  % Left color red, right color blue...

xlabel('iteration number')
ylabel('$N$')
% xlim([5e3 2e5])
xlim([-0.5, 11.5])


legend('$e_\infty(\sigma)$', '$e_1(\sigma)$', '$e_E(\sigma)$',...
       '$e_\infty(\vec{u})$', '$e_1(\vec{u})$', '$N$', 'Location', 'eastoutside')

% exportf(f1, [imagepath casename '_convergence.pdf']);