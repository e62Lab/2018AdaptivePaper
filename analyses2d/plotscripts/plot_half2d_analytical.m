close all

radius = 0.5;
eps = 0.001;

P = 1;

if 0

r = linspace(0, radius-eps, 300);
theta = linspace(0, 2*pi, 500);
[R, T] = meshgrid(r, theta);
X = R.*cos(T);
Y = R.*sin(T);


[SXX, SYY, SXY] = half2d_anal(X, Y, P, radius);
V = von_mises(SXX, SYY, SXY);

f1 = setfig('b1');
clevels = [linspace(0, 20, 40) linspace(20, 100, 60) linspace(100, 1000, 100)];
contourf(X, Y, V, clevels);
c = colorbar;
caxis([0, 20])
title(c, 'Pa', 'interpreter', 'LaTeX');
% l = get(c, 'Ytick');
% labels = cell(length(l), 1);
% for i = 1:length(l)
%     labels{i} = sprintf('%.2f', 10^l(i)-1);
% end
% set(c,'Ytick', l, 'YTicklabel', labels);
xlabel('$x$')
ylabel('$y$')
axis equal
colormap jet

end

eps = [0.2 0.02 0.002];

f2 = setfig('b2', [1200 350]);

for i = 1:3
    
y = linspace(0, radius-eps(i), 1000);
x = radius-eps(i)-y;
[sxx, syy, sxy] = half2d_anal(x, y, P, radius);
v = von_mises(sxx, syy, sxy);

subplot(1, 3, i); hold on; grid on; box on;
plot(y, sxx, 'LineWidth', 1.5)
plot(y, syy, 'LineWidth', 1.5)
plot(y, sxy, 'LineWidth', 1.5)
plot(y, v, 'LineWidth', 1.5)
legend('$\sigma_{xx}$', '$\sigma_{yy}$', '$\sigma_{xy}$', '$\sigma_{v}$',...
       'Location', 'NW');
ylabel('stress')
xlabel('$y,\ x = R-\gamma-y$')
xlim([0, radius-eps(i)])
title(sprintf('$\\gamma = %g$', eps(i)))

end

exportf(f2, [imagepath 'anal_cross.pdf'])