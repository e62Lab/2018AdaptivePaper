prepare

i = 1;
for c = [0.2, 0.02, 0.002]
    
s = sprintf('%g', c);
casename = ['half2d_convergence_moving_' s(3:end) '_v2_last_iter'];
load([plotdatapath casename '.mat'])

f1 = setfig(['bo' sprintf('%d', i)], [400, 400]);
scatter(x, y, 5, log10(err_ind), 'filled')
% legend('$\hat{e}$', 'Location', 'NE');
colormap jet
colorbar
xlim([-inf inf])
ylim([-inf, inf])
axis square
caxis([-9 -6])
title(sprintf('$\\gamma = %g$', eps));

% exportf(f1, [imagepath casename '.pdf'])

i = i + 1;
end
