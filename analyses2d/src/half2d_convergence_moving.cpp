#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"
#include "adaptive_solver.hpp"
#include "estimators.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

class SolverStress : public Solver {
  public:
    template <typename domain_t>
    Range<array<double, 3>> solve(domain_t& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        std::pair<Range<Vec2d>, Range<array<double, 3>>> res = Solver::solve(domain, conf, out_file, timer);
        return res.second;
    }
};


int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);
    SolverStress solver;
    solver.preprocess(conf);

    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file(output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);
    file.closeFile();

    omp_set_num_threads(conf.get<int>("params.sys.num_threads"));

    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");
    const int nx = conf.get<int>("params.num.nx");
    const double lam = conf.get<double>("params.case.lam");
    const double mu = conf.get<double>("params.case.mu");
    const double P = conf.get<double>("params.case.P");

    Timer t;
    t.addCheckPoint("begin");

    double dx = (R-eps)/nx;
    CircleDomain<Vec2d> domain = make_domain(conf, t, {0, 0}, R-eps, dx);

    auto anal_sol = [=](const Vec2d& p) { return analytical(p, P, R); };

    /*  // analytical solution check
    file.reopenFile();
    file.reopenFolder();
    file.setDomain("domain", domain);
    file.closeFile();

    int N = domain.size();
    Range<array<double, 3>> stress(N);
    for (int i = 0; i < N; ++i) {
        Matrix2d m = anal_sol(domain.positions[i]);
        stress[i][0] = m(0, 0);
        stress[i][1] = m(1, 1);
        stress[i][2] = m(0, 1);
    }
    file.reopenFile();
    file.reopenFolder();
    file.setDouble2DArray("stress", stress);
    file.closeFile(); */

//    string est_type = conf.get<string>("params.est.estimator_type");
//    if (est_type == "dev") {
//        DeviationEstimator estimator;
//        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
//    } else if (est_type == "anal"){
//        EnergyDensityEstimator<decltype(anal_sol)> estimator(anal_sol, lam, mu);
        L1Estimator<decltype(anal_sol)> estimator(anal_sol);
//        LinfEstimator<decltype(anal_sol)> estimator(anal_sol);
        auto largest = [=](const Vec2d& ) { return dx; };
        solve_with_estimator_move(conf, domain, solver, estimator, file, largest);
//    }

    file.closeFile();

    t.addCheckPoint("end");
    prn(t.getTime("begin", "end"));

    return 0;
}
