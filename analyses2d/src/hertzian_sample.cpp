#ifdef _OPENMP
#include <omp.h>
#endif
#include "hertzian_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "io.hpp"
#include "adaptive_solver.hpp"
#include "estimators.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);
    Solver::preprocess(conf);

    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file(output_file, HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);
    file.closeFile();

    #ifdef _OPENMP
    omp_set_num_threads(conf.get<int>("params.sys.num_threads"));
    #endif

    Timer t;
    t.addCheckPoint("domain");

    const double width = conf.get<double>("params.case.width");
    double dx = conf.get<double>("params.num.dx");

    RectangleDomain<Vec2d> domain = make_domain(conf, [=](const Vec2d&){ return dx; });

    int support_size = conf.get<int>("params.mls.n");
    FindBalancedSupport find_support(support_size, 2*support_size);
    domain.apply(find_support);

    prn(domain.size());

    file.reopenFile();
    file.openFolder("/");
    file.setDomain("domain", domain);
    file.closeFile();

    DeviationEstimator estimator;

    Solver solver;
    auto largest = [&](const Vec2d& v) { return dx*5; };
    solve_with_estimator_move(conf, domain, solver, estimator, file, largest);

    t.addCheckPoint("end");

    prn(t.getTime("domain", "end"));

    return 0;
}
