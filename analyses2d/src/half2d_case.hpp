#include "types.hpp"
#include "io.hpp"
#include "mlsm_operators.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_fill_engines.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace Eigen;
using namespace std;

Eigen::Matrix2d analytical(const Vec2d& p, double P, double R) {
    Matrix2d m;
    double x = p[0], y = p[1];
    double r1 = x*x + (R-y)*(R-y);
    double r2 = x*x + (R+y)*(R+y);
    double f = 2*P/M_PI;
    double sxx = -f*((R-y)*x*x/r1/r1 + (R+y)*x*x/r2/r2 - 1./R/2);
    double syy = -f*((R-y)*(R-y)*(R-y)/r1/r1 + (R+y)*(R+y)*(R+y)/r2/r2 - 1./R/2);
    double sxy = f*((R-y)*(R-y)*x/r1/r1 - (R+y)*(R+y)*x/r2/r2);
    m << sxx, sxy, sxy, syy;
    return m;
};

template <typename T>
T von_mises(T sxx, T syy, T sxy) {
    return std::sqrt(sxx*sxx - sxx*syy + syy*syy + 3*sxy*sxy);
}

template <typename scalar_t>
Eigen::VectorXd von_mises(const Range<std::array<scalar_t, 3>>& stress) {
    Eigen::VectorXd v(stress.size());
    for (int i = 0; i < stress.size(); ++i) {
        v[i] = von_mises(stress[i][0], stress[i][1], stress[i][2]);
    }
    return v;
}

template <typename T>
T energy_density_kernel(T sxx, T syy, T sxy, T lam, T mu) {
    T f = 4*mu*(lam+mu);
    return (lam*(2*sxy*sxy + (sxx-syy)*(sxx-sxy)) + 2*mu*(sxx*sxx+sxy*sxy+syy*syy)) / f;
}

template <typename scalar_t>
Eigen::VectorXd energy_density(const Range<std::array<scalar_t, 3>>& stress, scalar_t lam, scalar_t mu) {
    Eigen::VectorXd e(stress.size());
    for (int i = 0; i < stress.size(); ++i) {
        e[i] = energy_density(stress[i][0], stress[i][1], stress[i][2]);
    }
    return e;
}

template <typename domain_t, typename function_t>
void fill_domain_boundary(domain_t& domain, const function_t& f, double R) {
    Vec2d point(0, 0);
    domain.addBoundaryPoint(point, -4, Vec2d(-1, -1).normalized());
    point[0] += f(point);
    while (point[0] < R) {
        domain.addBoundaryPoint(point, -2, Vec2d(0, -1));
        point[0] += f(point);
    }
    point = {0, 0};
    point[1] += f(point);
    while (point[1] < R) {
        domain.addBoundaryPoint(point, -3, Vec2d(-1, 0));
        point[1] += f(point);
    }
    point = {0, R};
    double alpha = M_PI/2.0;
    alpha -= f(point) / R;
    double min_alpha = f(Vec2d(R, 0)) / R;
    while (alpha > min_alpha) {
        point = {std::cos(alpha), std::sin(alpha)};
        domain.addBoundaryPoint(R*point, -1, point);
        alpha -= f(R*point) / R;
    }
}

CircleDomain<Vec2d> make_domain(const XMLloader& conf, Timer& t,
                                const Vec2d& center, double r, const double dx) {
    CircleDomain<Vec2d> domain({0, 0}, r);
    double cutR = 1.1*r;
    RectangleDomain<Vec2d> cutout({-cutR, -cutR}, {cutR, 0});
    RectangleDomain<Vec2d> cutout2({-cutR, -cutR}, {0, cutR});
    domain.subtract(cutout);
    domain.subtract(cutout2);

    fill_domain_boundary(domain, [=](const Vec2d&) { return dx; }, r);

    int seed = conf.get<int>("params.num.seed");
    double pr = conf.get<double>("params.num.pr");
    PoissonDiskSamplingFill fill; fill.proximity_relax(pr).randomize(true).seed(seed);
    domain.apply(fill, dx);

    t.addCheckPoint("relax");
    int riter = conf.get<int>("params.num.riter");
    int num_neighbours = conf.get<int>("params.num.num_neighbours");
    double init_heat = conf.get<double>("params.num.init_heat");
    double final_heat = conf.get<double>("params.num.final_heat");
    BasicRelax relax;
    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
         .initialHeat(init_heat).finalHeat(final_heat);
    domain.apply(relax, dx);

    return domain;
}

template <typename domain_t, typename func_t>
void refill_with_distr(const XMLloader& conf, domain_t& domain, const func_t& dx) {
    double r = conf.get<double>("params.case.R") - conf.get<double>("params.case.eps");
    domain.clear();

    fill_domain_boundary(domain, dx, r);

    int seed = conf.get<int>("params.num.seed");
    double pr = conf.get<double>("params.num.pr");
    PoissonDiskSamplingFill fill; fill.proximity_relax(pr).randomize(true).seed(seed);
    domain.apply(fill, dx);

//    int riter = conf.get<int>("params.num.riter");
//    int num_neighbours = conf.get<int>("params.num.num_neighbours");
//    double init_heat = conf.get<double>("params.num.init_heat");
//    double final_heat = conf.get<double>("params.num.final_heat");
//    BasicRelax relax;
//    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
//            .initialHeat(init_heat).finalHeat(final_heat);
//    domain.apply(relax, dx);
}

class Solver {
  public:
    static void preprocess(XMLloader& conf) {
        double E = conf.get<double>("params.case.E");
        double nu = conf.get<double>("params.case.nu");

        // parameter logic
        double mu = E / 2. / (1+nu);
        double lam = E * nu / (1-2*nu) / (1+nu);
        auto state = conf.get<std::string>("params.case.state");
        assert_msg((state == "plane stress" || state == "plane strain"), "State is neither plane "
                "stress nor plane strain, but got '%s'.", state);
        if (state == "plane stress") {
            lam = 2 * mu * lam / (2 * mu + lam);  // plane stress
        }
        conf.set("params.case.mu", mu);
        conf.set("params.case.lam", lam);
    }

    template<typename vec_t, template<class> class domain_t>
    std::pair<Range<Vec2d>, Range<array<double, 3>>>
    solve(domain_t<vec_t>& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        int basis_size = conf.get<int>("params.mls.m");
        double basis_sigma = conf.get<double>("params.mls.sigmaB");
        double weight_sigma = conf.get<double>("params.mls.sigmaW");

        string basis_type = conf.get<string>("params.mls.basis_type");
        string weight_type = conf.get<string>("params.mls.weight_type");

        if (basis_type == "gau") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              Monomials<vec_t>(1));
            }
        } else if (basis_type == "mon") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              Monomials<vec_t>(1));
            }
        } else if (basis_type == "mon9") {
            Monomials<vec_t> basis({{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2}, {2,0}, {2,1}, {2,2}});
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, basis,
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, basis, Monomials<vec_t>(1));
            }
        } else if (basis_type == "mq") {
            MultiQuadric<Vec2d> basis(basis_size);
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, basis,
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, basis, Monomials<vec_t>(1));
            }
        }
        assert_msg(false, "Unknown basis type '%s' or weight type '%s'.", basis_type, basis_sigma);
    }

  private:
    template<typename domain_t, typename basis_t, typename weight_t>
    std::pair<Range<Vec2d>, Range<array<double, 3>>>
    solve_(domain_t& domain, const XMLloader& conf, HDF5IO& file, Timer& timer,
           const basis_t& basis, const weight_t& weight) {
        int N = domain.size();
        prn(N);
        int support_size = conf.get<int>("params.mls.n");

        Range<int> left = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[0]) < 1e-6; });
        domain.types[left] = -3;
        Range<int> bottom = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) < 1e-6; });
        domain.types[bottom] = -2;
        Range<int> center = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) + std::abs(v[0]) < 1e-6; });
        domain.types[center] = -4;

        const double E = conf.get<double>("params.case.E");
        const double v = conf.get<double>("params.case.nu");

        // Derived parameters
        double lam = E * v / (1 - 2 * v) / (1 + v);
        const double mu = E / 2 / (1 + v);
        lam = 2*mu*lam / (2*mu + lam);  // plane stress

        timer.addCheckPoint("shapes");

        auto mls = make_mls(basis, weight);
        RaggedShapeStorage<domain_t, decltype(mls), mlsm::all> storage(domain, mls, domain.types != 0, false);
        MLSM<decltype(storage)> op(storage);  // All nodes, including boundary
        SparseMatrix<double, RowMajor> M(2*N, 2*N);
        Range<int> ss = storage.support_sizes();
        ss.append(ss); for (int& c : ss) { c *= 2; }
        M.reserve(ss);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(2*N);

        timer.addCheckPoint("matrix");

        // Set equation on interior
        std::vector<int> interior = (domain.types > 0);
        for (int i : interior) {
            double x = domain.positions[i][0];
            double y = domain.positions[i][1];
            op.graddiv(M, i, lam+mu);
            op.lapvec(M, i, mu);
            rhs(i) = 0;
            rhs(i+N) = 0;
        }
        const double P = conf.get<double>("params.case.P");
        const double R = conf.get<double>("params.case.R");
        for (int i : domain.types == -1) {
            Vec2d normal = domain.normal(i);
            normal = (domain.positions[i]).normalized();
            op.traction(M, i, lam, mu, normal, 1.0);
            auto s = analytical(domain.positions[i], P, R);
            Vec2d t = s*normal;
            rhs(i) = t[0];
            rhs(i+N) = t[1];
//            rhs(i) = -normal[0]*domain.positions[i][1];
//            rhs(i+N) = -normal[1]*domain.positions[i][1];
        }
        for (int i : domain.types == -2) {
            op.der1(M, 0, 1, i, 1., 0);
            M.coeffRef(i+N, i+N) = 1;
            rhs(i) = 0;
            rhs(i+N) = 0;
        }
        for (int i : domain.types == -3) {
            M.coeffRef(i, i) = 1;
            op.der1(M, 1, 0, i, 1., 1);
            rhs(i) = 0;
            rhs(i+N) = 0;
        }
        for (int i : domain.types == -4) {
            M.coeffRef(i, i) = 1;
            M.coeffRef(i+N, i+N) = 1;
            rhs(i) = 0;
            rhs(i+N) = 0;
        }

//          file.reopenFile();
//          file.reopenFolder();
//          file.setSparseMatrix("M", M);
//          file.setDoubleArray("rhs", rhs);
//          file.closeFile();

//          SparseLU<SparseMatrix<double>> solver;

//          double droptol = conf.get<double>("params.solver.droptol");
//          int fill_factor = conf.get<int>("params.solver.fill_factor");
//          double errtol = conf.get<double>("params.solver.errtol");
//          int maxiter = conf.get<int>("params.solver.maxiter");
//          BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
//          solver.preconditioner().setDroptol(droptol);
//          solver.preconditioner().setFillfactor(fill_factor);
//          solver.setMaxIterations(maxiter);
//          solver.setTolerance(errtol);
        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        VectorXd sol = solver.solve(rhs);
        Range<Vec2d> displ = reshape<2>(sol.head(2*N));
        Range<array<double, 3>> stress(N);
        for (int i = 0; i < domain.size(); ++i) {
            auto grad = op.grad(displ, i);
            stress[i][0] = (2*mu + lam)*grad(0,0) + lam*grad(1,1);  // sxx
            stress[i][1] = lam*grad(0,0) + (2*mu+lam)*grad(1,1);    // syy
            stress[i][2] = mu*(grad(0,1)+grad(1,0));                // sxy
        }
        return {displ, stress};
    }
};
