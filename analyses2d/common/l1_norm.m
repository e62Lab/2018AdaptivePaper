function x = l1_norm(a, b, c)
if nargin == 1
    x = sum(abs(a)) / length(a);
elseif nargin == 2
    x = 1/2*(sum(abs(a)) / length(a) + sum(abs(b)) / length(b));
elseif nargin == 3
    x = 1/3*(sum(abs(a)) / length(a) + sum(abs(b)) / length(b) + sum(abs(c)) / length(c));
end
end