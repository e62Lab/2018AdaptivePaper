function [int, areas, values] = intTri3(trep, v)
P = trep.Points; T = trep.ConnectivityList;
a = P(T(:,2),:)-P(T(:,1),:);
b = P(T(:,3),:)-P(T(:,1),:);
c = P(T(:,4),:)-P(T(:,1),:);

volumes = abs(1/6*(...
    (-a(:,3).*b(:, 2) + a(:, 2).*b(:, 3)).*c(:, 1) + ...
    (a(:, 3).*b(:, 1) - a(:, 1).*b(:, 3)).*c(:, 2) + ...
    (-a(:, 2).*b(:, 1) + a(:, 1).*b(:, 2)).*c(:, 3)));

values = mean(v(T),2);
int = volumes'*values;
end
