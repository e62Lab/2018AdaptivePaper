function [ x ] = L1_norm_kernel(errsxx, errsyy, errsxy)
x = abs(errsxx) + abs(errsyy) + abs(errsxy);
end

