function [ sxx, syy, sxy ] = half2d_anal(x, y, P, R)
assert(R > 1e-6, 'R must be positive, got %f.', R);
assert(P > 1e-6, 'P must be positive, got %f.', P);
f = 2*P/pi;

r1 = x.^2 + (R-y).^2;
r2 = x.^2 + (R+y).^2;
sxx = -f*((R - y).*x.^2./r1.^2 + (R + y).*x.^2./r2.^2 - 1/2/R);
syy = -f*((R - y).^3./r1.^2 + (R + y).^3./r2.^2 - 1/2/R);
sxy = f*((R - y).^2.*x./r1.^2 - (R + y).^2.*x./r2.^2);

end