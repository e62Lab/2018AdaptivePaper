% --- Main script for generating the dithered trui image
close all ; clear all ;
ninit  = 1e4; % Upper bound on potential dot positions (PDPs) to use
dotmax = 5e+6 ; % Upper bound on number of dots to place
% --- Carry out the node placing
tic
xy = node_placing ([0 1 0 1],ninit,dotmax,@radius_trui) ;
toc
N = length(xy)
% --- Display the resulting dithered image
plot(xy(:,1),xy(:,2),'k.'); axis square