prepare

casename = 'adaptive_3d_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

a = h5readatt(datafile, '/conf', 'num.a');
P = h5readatt(datafile, '/conf', 'case.P');
E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');
e = h5readatt(datafile, '/conf', 'num.eps');

xd = linspace(-a, -e, 1000);
yd = xd; zd = xd;

[ud, vd, wd, sxxd, syyd, szzd, sxyd, sxzd, syzd] = point_contact_3d_analytical(xd, yd, yd, P, E, nu);
svd = von_mises(sxxd, syyd, szzd, sxyd, sxzd, syzd);
f1 = setfig('a1');
leg = {};
xlim([-a, -e]);
ylim([0, max(svd)+100]);
cmap = colormap('jet');

[X, Y, Z] = meshgrid(deal(linspace(-a, -e, 30)));

data = zeros(length(info.Groups), 6);
for i = 1:100
    name = sprintf('/%03d', i-1);

    try pos = h5read(datafile, [name '/domain/pos']); catch, break; end
    N = length(pos)
    x = pos(:, 1);
    y = pos(:, 2);
    z = pos(:, 3);
    types = h5read(datafile, [name '/domain/types']);
%     I = find(types < 0);
%     setfig('a4');
%     view(3)
%     daspect([1 1 1])
%     scatter3(x(I), y(I), z(I), 'filled');

    try sol = h5read(datafile, [name '/displ']); catch, break; end
    u = sol(:, 1);
    v = sol(:, 2);
    w = sol(:, 3);
    dnorm = sum(sol.^2, 2);

    stress = h5read(datafile, [name, '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    szz = stress(:, 3);
    sxy = stress(:, 4);
    sxz = stress(:, 5);
    syz = stress(:, 6);
    sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

    F = scatteredInterpolant(x, y, z, sv, 'linear');
    csvd = F(xd, yd, zd);
    figure(f1);
    plot(xd, csvd, 'o-', 'Color', cmap(5*i, :));
    leg{end+1} = sprintf('iter %d', i-1);

    ind = h5read(datafile, [name, '/error_indicator']);

%     setfig('a2'); view([   72.1000   26.8000]);
%     scatter3(x, y, z, 5, sv, 'filled');
%     colorbar
%     colormap jet
%     daspect([1 1 1])
%
    [au, av, aw, asxx, asyy, aszz, asxy, asxz, asyz] = point_contact_3d_analytical(x, y, z, P, E, nu);
    asv = von_mises(asxx, asyy, aszz, asxy, asxz, asyz);



    eu = abs(u-au); ev = abs(v-av); ew = abs(w-aw);
    esxx = abs(sxx - asxx);
    esyy = abs(syy - asyy);
    eszz = abs(szz - aszz);
    esxy = abs(sxy - asxy);
    esxz = abs(sxz - asxz);
    esyz = abs(syz - asyz);
    estress = [esxx; esyy; eszz; esxy; esxz; esyz];
    esv = abs(asv - sv);

    e1 = l1_norm(eu, ev, ew) / l1_norm(au, av, aw)
    esv = sum(esv) / sum(sv)


    data(i, 1) = N;
    data(i, 2) = e1;
    data(i, 3) = esv;

    try dx = h5read(datafile, [name, '/dx']); catch, break; end


end
fprintf('Finished after %d iterations.\n', i-1);

figure(f1);
plot(xd, svd, 'k-', 'LineWidth', 2);
leg{end+1} = 'analytical';
legend(leg, 'Location', 'NW');
xlim([-0.06 0])

setfig('b3');
plot(data(:, 1), data(:, 2), '-o');
plot(data(:, 1), data(:, 3), '-x');
legend('$e_1$ disp', '$e_1$ stress')
set(gca, 'xscale', 'log');
set(gca, 'yscale', 'log');
xlabel('$N$')
ylabel('error')

