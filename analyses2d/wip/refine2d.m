prepare

casename = 'refine2d';
datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = [];

% alpha = h5readatt(datafile, '/', 'alpha');
a = h5readatt(datafile, '/', 'a');
b = h5readatt(datafile, '/', 'b');
ax = linspace(0, 1, 100);
ay = linspace(0, 1, 100);
[X, Y] = meshgrid(ax, ay);
% asol = analytical1d(ax, alpha);
asol = analytical2d(X, Y, a, b);

for iter = 0:10
    name = sprintf('/iter%04d', iter);
    
    try
        N = h5readatt(datafile, name, 'N');
    catch e
        fprintf('Done at iter %d.\n', iter);
        break
    end
    pos = h5read(datafile, [name '/pos']);
%     types = h5read(datafile, [name '/types']);

    x = pos(1, :);
    y = pos(2, :);

    sup = h5read(datafile, [name '/support'])+1;
    try
    sol = h5read(datafile, [name '/sol']);
    catch
        break
    end
%     imb = h5read(datafile, [name '/imb']);
%      imb(imb < 3) = 0;
%     imb(imb > 7) = 7;
% 
    
%     err_mat = max(abs(analytical1d(x, alpha) - sol'))
    err_mat = max(abs(analytical2d(x, y, a, b) - sol'))
    err = analytical2d(x, y, a, b) - sol';

    err_ind = h5read(datafile, [name '/error_indicator']);
    
    M = spconvert(h5read(datafile, [name '/M'])');
    rhs = h5read(datafile, [name '/rhs']);
%     sol2 = M \ rhs;
%     error = norm(sol-sol2)/norm(sol);
%     assert(error < 1e-10, 'Matlab and C++ solutions do not agree, err=%f', error);

    data(iter+1, 1) = iter+1;
    data(iter+1, 2) = max(abs(err_mat));
    data(iter+1, 3) = length(x);
    
    f1 = setfig(loc8{iter+1}, []);
%     linkdata on
    z = zeros(size(x));
    try
        idx = 520;
        sup(:, idx);
        z(sup(:, idx)) = 1;
        z(idx) = 2;
    catch
    end
    
    h = scatter(x, y, 15, abs(err), 'filled');
    title(sprintf('iter \\#%d', iter+1));
%     caxis([0, 1e-2])
    xlim([0, 1])
    ylim([0, 1])
    daspect([1 1 1])
    colorbar
    colormap jet
    
    explore_domain(f1, h, pos, sup);
    
    iter=iter
%     break
end

f2 = setfig('b4', []);
plot(data(:, 1), data(:, 2), '-o')
set(gca, 'YScale', 'log');
ylabel('$L_\infty$ error')
xlabel('iteration')
yyaxis right
plot(data(:, 1), data(:, 3), '-o')
set(gca, 'YScale', 'log');
ylabel('$N$')


