prepare

datafile = [datapath 'refine1d.h5'];
info = h5info(datafile);

data = [];

alpha = h5readatt(datafile, '/', 'alpha');
ax = linspace(0, 1, 1000);
asol = analytical1d(ax, alpha);

for iter = 0:15
    name = sprintf('/iter%04d', iter);
    
    try
        N = h5readatt(datafile, name, 'N');
    catch e
        fprintf('Done at iter %d.\n', iter);
        break
    end
    pos = h5read(datafile, [name '/pos']);
    x = pos(1, :);

    sup = h5read(datafile, [name '/support'])+1;

    
    f1 = setfig(loc8{iter+1});
    z = zeros(size(x));
    try
        idx = 3;
        support = sup(:, idx);
        z(support(support~=0)) = 1;
        z(idx) = 2;
    catch
    end
    
    scatter(x, 3*ones(size(x)), 15, z, 'filled')
    plot(ax, asol, '-')
    title(sprintf('iter \\#%d', iter+1));
    
        
    M = spconvert(h5read(datafile, [name '/M'])');
    rhs = h5read(datafile, [name '/rhs']);
    sol = M \ rhs;
    
    sol = h5read(datafile, [name '/sol']);
    
    err_mat = max(abs(analytical1d(x, alpha) - sol'))
        
    err_ind = h5read(datafile, [name '/error_indicator']);
    
    plot(x, sol, 'o')
    ylim([-0.5, 3])
    
    
    data(iter+1, 1) = iter+1;
    data(iter+1, 2) = max(abs(err_mat));
    data(iter+1, 3) = length(x);
    
    iter=iter
%     break
end

f2 = setfig('b4');
plot(data(:, 1), data(:, 2), '-o')
set(gca, 'YScale', 'log');
ylabel('$L_\infty$ error')
xlabel('iteration')
yyaxis right
plot(data(:, 1), data(:, 3), '-o')
set(gca, 'YScale', 'log');
ylabel('$N$')


