prepare

datafile = [datapath 'refine1d.h5'];
info = h5info(datafile);
alpha = h5readatt(datafile, '/', 'alpha');


data = [];
name = '/iter0000';
N = h5readatt(datafile, name, 'N');
pos = h5read(datafile, [name '/pos']);
x = pos(1, :);

sup = h5read(datafile, [name '/support'])+1;

sol = h5read(datafile, [name '/sol']);
err = h5read(datafile, [name '/error']);

f1 = setfig('b1');
% z = zeros(size(x));
% idx = 41;
% z(sup(:, idx)) = 1;
% z(idx) = 2;
% scatter(x, sol, 5, sol, 'filled');
% colorbar
% colormap jet
plot(x, sol, 'o')
ax = linspace(0, 1, 1000);
asol = analytical(ax, alpha);
plot(ax, asol, '-')
%     caxis([0, 1e-2])
%     break