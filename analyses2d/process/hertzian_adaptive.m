prepare

casename = 'hertzian2';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

name = '/';

a = h5readatt(datafile, '/conf', 'case.a');
p0 = h5readatt(datafile, '/conf', 'phy.p0');

lam = h5readatt(datafile, '/conf', 'phy.lam');
mu = h5readatt(datafile, '/conf', 'phy.mu');

data = cell(4, 1);

[X, Y] = meshgrid(linspace(-3*a, 3*a, 600), linspace(-3*a, 0, 300));
[ASXX, ASYY, ASXY] = hertzian_analytical(X, Y, a, p0);

for iter = 0:6

name = sprintf('/iter%04d', iter);

try
pos = h5read(datafile, [name '/domain/pos']);
catch, break, end
x = pos(1, :)';
y = pos(2, :)';
N = length(x)
types = h5read(datafile, [name '/domain/types']);

displ = h5read(datafile, [name '/displ']);

uu = displ(1, :);
vv = displ(2, :);
dnorm = sqrt(uu.^2 + vv.^2);
stress = h5read(datafile, [name '/stress']);
sxx = stress(1, :);
syy = stress(2, :);
sxy = stress(3, :);
v = von_mises(sxx, syy, sxy);

I = -5*a < x & x < 5*a & -5*a < y; 
if sum(I) < 7
    I = -20*a < x & x < 20*a & -20*a < y; 
end

SXX = griddata(x, y, sxx, X, Y);
SXY = griddata(x, y, sxy, X, Y);
SYY = griddata(x, y, syy, X, Y);

EXX = abs(SXX-ASXX);
EXY = abs(SXY-ASXY);
EYY = abs(SYY-ASYY);

e = energy_norm_kernel(EXX, EYY, EXY, lam, mu);
total = energy_norm_kernel(ASXX, ASYY, ASXY, lam, mu);
errE = sqrt(sum(sum(e)) / sum(sum(total)));

errinf = max([max(max(EXX)), max(max(EXY)), max(max(EYY))])/p0;
err1 = mean([mean(mean(EXX)), mean(mean(EXY)), mean(mean(EYY))])/p0;

data{iter+1}.N = N;
data{iter+1}.x = x(I);
data{iter+1}.y = y(I);
data{iter+1}.uu = uu(I);
data{iter+1}.vv = vv(I);
data{iter+1}.sxx = sxx(I);
data{iter+1}.syy = syy(I);
data{iter+1}.sxy = sxy(I);
data{iter+1}.errE = errE;
data{iter+1}.errinf = errinf;
data{iter+1}.err1 = err1;

end

save([plotdatapath casename '.mat'], 'data', 'a', 'p0', 'lam', 'mu')