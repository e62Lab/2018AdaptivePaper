prepare

casename = 'half2d_convergence_moving_2_v2';
datafile = [datapath casename '.h5'];
info = h5info(datafile);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

name = info.Groups(end).Name;
N = h5readatt(datafile, name, 'N');
pos = h5read(datafile, [name '/domain/pos']);
types = h5read(datafile, [name '/domain/types']);

x = pos(1, :);
y = pos(2, :);

sup = h5read(datafile, [name '/domain/supp'])+1;
sol = h5read(datafile, [name '/sol']);
sxx = sol(1, :);
syy = sol(2, :);
sxy = sol(3, :);
v = von_mises(sxx, syy, sxy);
[asxx, asyy, asxy] = half2d_anal(x, y, P, R);
av = von_mises(asxx, asyy, asxy);
errsxx = sxx - asxx; errsyy = syy - asyy; errsxy = sxy - asxy;

max_err = max([abs(errsxx); abs(errsyy); abs(errsxy)]);

err_ind = h5read(datafile, [name '/error_indicator']);
    

l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);

T = delaunayTriangulation([double(x)' double(y)']);
e = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
err = sqrt(intTri(T, e) / intTri(T, total));

L1kernel = L1_norm_kernel(errsxx, errsyy, errsxy);
total = L1_norm_kernel(asxx, asyy, asxy);
L1 = intTri(T, L1kernel) / intTri(T, total);
    
save([plotdatapath casename '_last_iter.mat'], 'x', 'y', 'err_ind', 'eps')