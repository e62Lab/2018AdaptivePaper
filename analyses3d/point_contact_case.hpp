#ifndef ADAPTIVE3D_POINT_CONTACT_CASE_HPP
#define ADAPTIVE3D_POINT_CONTACT_CASE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;

struct PointContactSolver {

    static Vec3d analytical(const Vec3d& p, double P, double E, double nu) {
        double x = p[0], y = p[1], z = p[2];
        double r = std::sqrt(x*x+y*y);
        double c = x/r, s = y/r;
        double R = p.norm();
        double mu = E / 2 / (1+nu);
        double u = P*r/4/PI/mu * (z/R/R/R - (1-2*nu) / (R*(R+z)));
        double w = P/4/PI/mu * (z*z/R/R/R + 2*(1-nu)/R);
        return {u*c, u*s, w};
    }

    static void preprocess(XML& conf) {
        double E = conf.get<double>("case.E");
        double nu = conf.get<double>("case.nu");

        // parameter logic
        double mu = E / 2. / (1+nu);
        double lam = E * nu / (1-2*nu) / (1+nu);
        conf.set("case.mu", mu);
        conf.set("case.lam", lam);
    }

    template<typename vec_t>
    std::pair<VectorField3d, VectorField<double, 6>>
    solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& out_file, Timer& timer) {
        int basis_size = conf.get<int>("mls.m");
        double basis_sigma = conf.get<double>("mls.sigmaB");
        double weight_sigma = conf.get<double>("mls.sigmaW");

        string basis_type = conf.get<string>("mls.basis_type");
        string weight_type = conf.get<string>("mls.weight_type");

        if (basis_type == "gau") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, Gaussians<vec_t>(basis_size, basis_sigma),
                              GaussianWeight<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, Gaussians<vec_t>(basis_size, basis_sigma),
                              NoWeight<vec_t>());
            }
        } else if (basis_type == "mon") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              GaussianWeight<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              NoWeight<vec_t>());
            }
        } else if (basis_type == "mon9") {
            Monomials<vec_t> basis = Monomials<vec_t>::tensorBasis(2);
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, basis,
                              GaussianWeight<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, basis, NoWeight<vec_t>());
            }
        }
        assert_msg(false, "Unknown basis type '%s' or weight type '%s'.", basis_type, basis_sigma);
        throw "";
    }

    template<typename vec_t, typename basis_t, typename weight_t>
    std::pair<VectorField3d, VectorField<double, 6>>
    solve_(DomainDiscretization<vec_t>& d, const XML& conf, HDF& out_file, Timer& timer, const basis_t& basis, const weight_t& weight) {

        int N = d.size();
        prn(N);

        double P = conf.get<double>("case.P");
        double E = conf.get<double>("case.E");
        double nu = conf.get<double>("case.nu");
        double lam = conf.get<double>("case.lam");
        double mu = conf.get<double>("case.mu");

        timer.addCheckPoint("shapes");
        prn("shapes");

        WLS<basis_t, weight_t, ScaleToFarthest> wls(basis, weight);

        auto storage = d.computeShapes(wls);

        timer.addCheckPoint("matrix");
        prn("matrix");

        const int dim = vec_t::dim;
        SparseMatrix<double, RowMajor> M(dim*N, dim*N);
        Range<int> ss = storage.supportSizes();
        ss.append(ss+ss); for (int& c : ss) { c *= dim; }
        M.reserve(ss);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dim*N);

        auto op = storage.implicitVectorOperators(M, rhs);

        for (int i : d.interior()) {
            (lam+mu)*op.graddiv(i) + mu*op.lap(i) = 0;
        }
        for (int i : d.boundary()) {
            op.value(i) = analytical(d.pos(i), P, E, nu);
        }

//        out_file.atomic().writeSparseMatrix("M", M);
//        out_file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint("postprocess");
        VectorField3d disp = VectorField3d::fromLinear(sol);
        VectorField<double, 6> stress(N);
        auto eop = storage.explicitVectorOperators();
        for (int i = 0; i < N; ++i) {
            auto grad = eop.grad(disp, i);
            Matrix3d eps = 0.5*(grad + grad.transpose());
            Matrix3d s = lam * eps.trace() * Matrix3d::Identity(3, 3) + 2*mu*eps;
            stress[i][0] = s(0, 0);
            stress[i][1] = s(1, 1);
            stress[i][2] = s(2, 2);
            stress[i][3] = s(0, 1);
            stress[i][4] = s(0, 2);
            stress[i][5] = s(1, 2);
        }
        timer.addCheckPoint("end");

        out_file.atomic().writeEigen("displ", disp);
        out_file.atomic().writeEigen("stress", stress);

        return {disp,stress};
    }

};


#endif //ADAPTIVE3D_POINT_CONTACT_CASE_HPP
