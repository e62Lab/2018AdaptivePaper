#include "point_contact_case.hpp"

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    PointContactSolver solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    double a = conf.get<double>("num.a");
    double eps = conf.get<double>("num.eps");
    BoxShape<Vec3d> box(-a, -eps);

    vector<string> ns = split(conf.get<string>("num.nxs"), ",");
    int iter = 0;
    for (const string& s : ns) {
        int nx = stoi(s);
        cout << "-------- " << nx << " -----------\n";
        file.setGroupName(format("%02d", iter));

        Timer timer;
        timer.addCheckPoint("domain");

        double dx = 1.0/nx;
        GeneralFill<Vec3d> fill; fill.seed(1337);
        DomainDiscretization<Vec3d> d = box.discretizeWithDensity([=](const Vec3d&) { return dx; }, &fill);

        int ss = conf.get<int>("mls.n");
        d.findSupport(FindClosest(ss));

        file.atomic().writeDomain("domain", d);

        solver.solve(d, conf, file, timer);

        ++iter;
    }

    return 0;
}
